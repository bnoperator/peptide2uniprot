ffilter = function(df, dupaction, filterqt){
  fdf = subset(df, QuantitationType == filterqt)
  if(dupaction == "Min"){
    idx = which.min(fdf$value)
  } else if(dupaction == "Max") {
    idx = which.max(fdf$value)
  } else {
    idx = which.min( (fdf$value-mean( .$value ) )^2 )
  }
  df = df %>% group_by(QuantitationType) %>% do({
    value = .$value
    value[1:length(value) != idx] = NaN
    res = data.frame(rowSeq = .$rowSeq, colSeq = .$colSeq, value = value)
  })
  df = subset(df, !is.nan(value))
  return(df)
}
